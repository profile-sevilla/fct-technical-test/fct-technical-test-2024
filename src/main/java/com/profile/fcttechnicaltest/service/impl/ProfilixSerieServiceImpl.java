package com.profile.fcttechnicaltest.service.impl;

import com.profile.fcttechnicaltest.model.ProfilixSerieDto;
import com.profile.fcttechnicaltest.repository.ProfilixSerieRepository;
import com.profile.fcttechnicaltest.service.ProfilixSerieService;
import com.profile.fcttechnicaltest.util.ProfilixSerieMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfilixSerieServiceImpl implements ProfilixSerieService {

    @Autowired
    ProfilixSerieRepository profilixSerieRepository;
    @Override
    public ProfilixSerieDto getSerie() {
        // This is an example of getting data from a repository and converting the result to DTO with a mapper.
        // Further changes may be done in the mapper classes.
        return ProfilixSerieMapper.profilixSerieMapperEntityToDto(profilixSerieRepository.getById(1));
    }
}
