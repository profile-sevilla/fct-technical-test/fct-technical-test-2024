package com.profile.fcttechnicaltest.service;

import com.profile.fcttechnicaltest.model.ProfilixSerieDto;

public interface ProfilixSerieService {
    ProfilixSerieDto getSerie();
}
