package com.profile.fcttechnicaltest.repository;

import com.profile.fcttechnicaltest.model.domain.ProfilixSerieEpisode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfilixSerieEpisodeRepository extends JpaRepository<ProfilixSerieEpisode, Integer> {
}
