package com.profile.fcttechnicaltest.repository;

import com.profile.fcttechnicaltest.model.domain.ProfilixSerie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfilixSerieRepository extends JpaRepository<ProfilixSerie, Integer> {
}
