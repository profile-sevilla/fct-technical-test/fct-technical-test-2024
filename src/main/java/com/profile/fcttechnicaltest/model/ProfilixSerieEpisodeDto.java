package com.profile.fcttechnicaltest.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@Builder
public class ProfilixSerieEpisodeDto {
    @NotBlank
    private String title;
    @NotBlank
    private double duration;
}
