package com.profile.fcttechnicaltest.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@Builder
public class ProfilixSerieDto {
    @NotBlank
    private String title;
    @NotBlank
    private Integer seasons;
    @NotBlank
    private List<ProfilixSerieEpisodeDto> episodes;
}