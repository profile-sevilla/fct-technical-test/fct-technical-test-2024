package com.profile.fcttechnicaltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FctTechnicalTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(FctTechnicalTestApplication.class, args);
    }

}
