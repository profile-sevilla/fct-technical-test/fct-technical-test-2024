package com.profile.fcttechnicaltest.config;

import com.profile.fcttechnicaltest.model.domain.ProfilixSerie;
import com.profile.fcttechnicaltest.model.domain.ProfilixSerieEpisode;
import com.profile.fcttechnicaltest.repository.ProfilixSerieRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class DatabaseInit {
    @Autowired
    public ProfilixSerieRepository profilixSerieRepository;

    @PostConstruct
    public void createSeries() {
        List<ProfilixSerieEpisode> episodes = new ArrayList<>();
        ProfilixSerieEpisode episode = new ProfilixSerieEpisode(null, "test-episode", 1);
        episodes.add(episode);
        ProfilixSerie serie1 = new ProfilixSerie(1,"Wednesday", 1, episodes);
        ProfilixSerie serie2 = new ProfilixSerie(2, "Friends", 13, episodes);
        ProfilixSerie serie3 = new ProfilixSerie(3, "Peaky Blinders", 5, episodes);
        log.info("Inserting tv series");
        profilixSerieRepository.save(serie1);
        profilixSerieRepository.save(serie2);
        profilixSerieRepository.save(serie3);
    }
}