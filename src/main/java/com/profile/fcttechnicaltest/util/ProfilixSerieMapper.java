package com.profile.fcttechnicaltest.util;

import com.profile.fcttechnicaltest.model.ProfilixSerieDto;
import com.profile.fcttechnicaltest.model.domain.ProfilixSerie;

public class ProfilixSerieMapper {

    public static ProfilixSerieDto profilixSerieMapperEntityToDto(ProfilixSerie serie) {
        return ProfilixSerieDto.builder()
                .title(serie.getTitle())
                .seasons(serie.getSeasons())
                .episodes(ProfilixSerieEpisodeMapper.mapEpisodesFromEntityToDto(serie.getEpisodes()))
                .build();
    }

    public static ProfilixSerie profilixSerieMapperDtoToEntity(ProfilixSerieDto serieDto) {
        return ProfilixSerie.builder()
                .title(serieDto.getTitle())
                .seasons(serieDto.getSeasons())
                .episodes(ProfilixSerieEpisodeMapper.mapEpisodesFromDtoToEntity(serieDto.getEpisodes()))
                .build();
    }

}
