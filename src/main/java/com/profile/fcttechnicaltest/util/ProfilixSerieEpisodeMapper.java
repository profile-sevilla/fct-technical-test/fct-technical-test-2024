package com.profile.fcttechnicaltest.util;

import com.profile.fcttechnicaltest.model.ProfilixSerieEpisodeDto;
import com.profile.fcttechnicaltest.model.domain.ProfilixSerieEpisode;

import java.util.ArrayList;
import java.util.List;

public class ProfilixSerieEpisodeMapper {

    public static ProfilixSerieEpisodeDto profilixSerieEpisodeMapperEntityToDto(ProfilixSerieEpisode serieEpisode) {
        return ProfilixSerieEpisodeDto.builder()
                .title(serieEpisode.getTitle())
                .duration(serieEpisode.getDuration())
                .build();
    }

    public static ProfilixSerieEpisode profilixSerieEpisodeMapperDtoToEntity(ProfilixSerieEpisodeDto serieEpisodeDto) {
        return ProfilixSerieEpisode.builder()
                .title(serieEpisodeDto.getTitle())
                .duration(serieEpisodeDto.getDuration())
                .build();
    }

    public static List<ProfilixSerieEpisodeDto> mapEpisodesFromEntityToDto(List<ProfilixSerieEpisode> episodes) {
        List<ProfilixSerieEpisodeDto> episodeDtos = new ArrayList<>();
        for (ProfilixSerieEpisode entityEmpisode : episodes) {
            episodeDtos.add(profilixSerieEpisodeMapperEntityToDto(entityEmpisode));
        }
        return episodeDtos;
    }

    public static List<ProfilixSerieEpisode> mapEpisodesFromDtoToEntity(List<ProfilixSerieEpisodeDto> episodesDto) {
        List<ProfilixSerieEpisode> episodes = new ArrayList<>();
        for (ProfilixSerieEpisodeDto episodeDto : episodesDto) {
            episodes.add(profilixSerieEpisodeMapperDtoToEntity(episodeDto));
        }
        return episodes;
    }
}
