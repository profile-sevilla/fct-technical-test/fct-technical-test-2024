package com.profile.fcttechnicaltest.controller;

import com.profile.fcttechnicaltest.model.ProfilixSerieDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/serie")
public interface ProfilixSerieApi {

    @GetMapping
    ProfilixSerieDto getSerie();
}