package com.profile.fcttechnicaltest.controller;

import com.profile.fcttechnicaltest.model.ProfilixSerieDto;
import com.profile.fcttechnicaltest.service.ProfilixSerieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProfilixSerieController implements ProfilixSerieApi {

    @Autowired
    ProfilixSerieService profilixSerieService;

    @Override
    public ProfilixSerieDto getSerie() {
        return profilixSerieService.getSerie();
    }
}
