# fct-technical-test

### **Description**

This is a basic entrance test to join our crew for the training period.
The technologies used in this project are the following:


1. Maven (Used for the dependency management)
1. Spring Boot (Make it easy to boot and deploy the application and its configuration)
1. Spring Data JPA (To access the database)
1. Spring MVC (which defines the architecture of a Rest application)
1. Lombok (Make it easy to create the model of our application)
1. H2 Database (Runtime SQL database)


### **Mandatory**
In this example, we already have some stuff codded to develop a new TV streaming service called Profilix (Yes, we are so creative), but there are several TODO tasks in our backlog yet:

1. Create a new Profilix Series in the database using the correct HTTP method.
1. Create a new Profilix Series Episode (or many) for an existing Profilix Series in the database using the correct HTTP method.
1. Obtain the Profilix Series with all its episodes from the database using the correct HTTP method.
1. Delete a Profilix Series and all its Profilix Series Episodes from the database using the correct HTTP method.
1. Filter the Profilix Series by name.

### **Extra points**
This extra task will be highly valued **only if the mandatory** tasks have been completed:
1. Create a new User entity with a few characteristic attributes and a relation of "wish list to view" with Profilix Series.
1. Create User CRUD methods to persist and consult from/into the database with the correct HTTP methods.
1. Create CRUD methods to add, get and remove Profilix Series from User "wish list to view" with the correct HTTP methods.

Feel free to add as much data and layers as you need to achieve the previous tasks described above.


### Way of working in order to commit a valid test

1. **Each student must create a private fork of the project, to which access will be granted to [@amarin](https://gitlab.com/amarin_profile).**
<br>This step is the most important in order to have your code reviewed

1. **Document the working process. (It's recommendable to do this using the project Wiki)**
<br>The documentation should have links or references to the websites or articles that you have used during the working process

1. **Entity-DTO and DTO-entity conversions must be done in the services implementations**
<br>You can find an example in the **ProfilixSerieServiceImpl** class.

1. **Do atomic commits.**
<br>All commit messages must be clear and relate to the code committed

1. **Code must be in English**
<br>Everything must be coded in English, and the names of variables and methods must have a sense related to what they do/are.

**NOTE: As the owner of the repository, we can see if you give access to
other users to share your code.**

**Deadline for delivery is ```16/01/2024```**.

### Project config and running settings

**Requirements to compile and run the project**

To use Maven, it is necessary to configure **JAVA_HOME**, download Maven from [here](http://maven.apache.org/download.cgi) and unzip the folder.

The next step is to add **MAVEN_HOME** as a system variable and point it to the Maven folder, which is also necessary
add **%MAVEN_HOME%\bin** to path in the system variable.

To compile, you must use ```mvn clean install```. This command cleans the target, compiles the project,
executes the tests and installs them in your local repository.

To start the project, you must execute the main class
```FctTechnicalTestApplication.java```

It is mandatory to use JDK 17 64 bits. For download, click [here](https://jdk.java.net/archive/)

#### Working with GIT

Firstly, do a fork to have your own copy of our repository in yours. Then clone the project from your fork into your computer with the following
command:
``` script 
git clone HTTPS_URL
```
Go to the folder that contains the project and create a develop branch. In this branch, you will be committing your changes.
 ``` script 
git checkout develop
```
To commit your files, add the changes.
 ``` script 
git add .
```
Then write a message with the information about the changes that you have made.
To commit and save in the HEAD of your local copy.
 ``` script 
git commit -m "Commit message"
```
To complete the process, it is necessary to send the changes to your remote repository
 ``` script 
git push origin develop
```

### **Links of interest**

- [StackOverflow](https://es.stackoverflow.com)
- [Baeldung](https://www.baeldung.com/)
- [DZone](https://dzone.com/)
- [Medium](https://medium.com/)
- [GIT cheat sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf)

